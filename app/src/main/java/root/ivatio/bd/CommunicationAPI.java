package root.ivatio.bd;

public interface CommunicationAPI {
    int COMMUNICATION = 0;
    int COMMUNICATION_KEY = 1;
    int getType();
    long getID();
}
